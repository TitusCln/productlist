package com.mobile.tituscln.productlist.login.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.mobile.tituscln.productlist.login.model.LoginRequest
import com.mobile.tituscln.productlist.login.model.UserApi
import com.mobile.tituscln.productlist.utils.SingleLiveEvent
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug

class LoginViewModel : ViewModel(), AnkoLogger {


    val webService by lazy {
        UserApi.create()
    }
    var disposable: Disposable? = null
    private val tokenAccess = MutableLiveData<String>()
    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val error = SingleLiveEvent<String>()

    fun getAccessToken(): LiveData<String> {
        return tokenAccess
    }

    fun get() = error

    fun login() {
        disposable = webService.login(LoginRequest(email = username.value, password = password.value)).subscribeOn(Schedulers.io())
                ?.subscribe(
                        { loginResponse ->
                            debug("Entro a la respuesta ${loginResponse.user} ")
                            tokenAccess.postValue(loginResponse.accessToken)
                        },
                        { errors ->
                            error.postValue(errors.localizedMessage)
                        })
    }
}