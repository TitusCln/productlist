package com.mobile.tituscln.productlist.product.ui

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mobile.tituscln.productlist.R
import com.mobile.tituscln.productlist.product.viewmodel.ProductsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.longToast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val token = intent.extras.getString("token", "")
        longToast("el token es $token").show()

        val viewModel = ViewModelProviders.of(this).get(ProductsViewModel::class.java)
        viewModel.bearer.value = token
        fab.setOnClickListener { view ->
            startActivity(intentFor<AddProductActivity>("token" to token))
        }
    }

}
