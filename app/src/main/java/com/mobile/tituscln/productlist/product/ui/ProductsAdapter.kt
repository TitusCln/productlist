package com.mobile.tituscln.productlist.product.ui

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.mobile.tituscln.productlist.R
import com.mobile.tituscln.productlist.product.model.Product
import com.mobile.tituscln.productlist.utils.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*
import org.greenrobot.eventbus.EventBus

class ProductsAdapter(val list: List<Product>) : RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder = ProductViewHolder(parent.inflate(R.layout.item_product))

    override fun getItemCount(): Int = list.size


    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class ProductViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {


        fun bind(product: Product) {
            product.let {
                itemView?.let {
                    itemView.product_title.text = product.name
                    itemView.product_description.text = product.desc
                    try {
                        Picasso.get().load(product.imgUrl).placeholder(R.drawable.placeholder).into(itemView.product_image)
                    } catch (ex: Exception) {

                    }
                    itemView.product_delete.setOnClickListener {
                        EventBus.getDefault().post(product.id)
                    }
                }

            }
        }
    }
}