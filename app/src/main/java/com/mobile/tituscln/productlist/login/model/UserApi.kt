package com.mobile.tituscln.productlist.login.model

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST


interface UserApi {
    @POST("/login")
    fun login(@Body user: LoginRequest): Observable<LoginResponse>

    companion object {
        fun create(): UserApi {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://192.168.9.29:5000/")
                    .build()
            return retrofit.create(UserApi::class.java)
        }

    }

}