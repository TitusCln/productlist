package com.mobile.tituscln.productlist.login.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity
data class User(
        @PrimaryKey
        val email: String? = null,
        val name: String? = null,
        val id: Int? = null,
        val encryptedPassword: String? = null
)

data class LoginResponse(
        val accessToken: String? = null,
        val user: User? = null
)

data class LoginRequest(val email: String? = null, val password: String? = null)