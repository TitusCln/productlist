package com.mobile.tituscln.productlist.product.model

import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ProductApi {

    @GET("/products")
    fun getAllProducts(): Observable<List<Product>>

    @DELETE("/products/{productId}")
    fun deleteProduct(@Path("productId") id: Int?): Observable<Boolean>

    @POST("/products/")
    fun addProduct(@Body product: Product): Observable<Product>

    companion object {

        fun create(bearer: String): ProductApi {
            val client = OkHttpClient.Builder().addInterceptor { chain ->
                val newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + bearer)
                        .build()
                chain.proceed(newRequest)
            }.build()

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://192.168.9.29:5000/")
                    .client(client)
                    .build()

            return retrofit.create(ProductApi::class.java)
        }

    }
}