package com.mobile.tituscln.productlist.login.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mobile.tituscln.productlist.R
import com.mobile.tituscln.productlist.databinding.ActivityLoginBinding
import com.mobile.tituscln.productlist.login.viewmodel.LoginViewModel
import com.mobile.tituscln.productlist.product.ui.MainActivity
import com.mobile.tituscln.productlist.utils.afterTextChanged
import com.mobile.tituscln.productlist.utils.simpleDialog
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.intentFor

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        val viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        binding.let {
            it.viewModel = viewModel
            it.setLifecycleOwner(this)
        }

        viewModel.getAccessToken().observe(this, Observer { accessToken ->
            if (!accessToken.isNullOrEmpty()) {
                startActivity(intentFor<MainActivity>("token" to accessToken))
                finish()
            }
        })
        viewModel.error.observe(this, Observer { error ->
            simpleDialog(R.string.error_message, error, R.string.ok)
        })

        password.afterTextChanged { viewModel.password.value = it }
        email.afterTextChanged { viewModel.username.value = it }
        email_sign_in_button.setOnClickListener {
            viewModel.login()
        }
    }


}
