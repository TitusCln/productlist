package com.mobile.tituscln.productlist.product.model

import android.arch.persistence.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class Product(

        @field:SerializedName("imgUrl")
        val imgUrl: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("desc")
        val desc: String? = null
)