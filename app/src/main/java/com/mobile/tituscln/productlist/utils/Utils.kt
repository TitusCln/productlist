package com.mobile.tituscln.productlist.utils

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import com.mobile.tituscln.productlist.R

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun ViewGroup.inflate(layoutId: Int): View {
    return LayoutInflater.from(context).inflate(layoutId, this, false)
}

fun Context.simpleDialog(titleRes: Int, messageRes: Int, okRes: Int) {
    MaterialDialog.Builder(this)
            .titleGravity(GravityEnum.CENTER)
            .contentGravity(GravityEnum.CENTER)
            .buttonsGravity(GravityEnum.CENTER)
            .title(titleRes)
            .content(messageRes)
            .neutralText(okRes)
            .neutralColorRes(R.color.colorAccent)
            .show()
}

fun Context.simpleDialog(titleRes: String, messageRes: String, okRes: String) {
    MaterialDialog.Builder(this)
            .titleGravity(GravityEnum.CENTER)
            .contentGravity(GravityEnum.CENTER)
            .buttonsGravity(GravityEnum.CENTER)
            .title(titleRes)
            .content(messageRes)
            .neutralText(okRes)
            .neutralColorRes(R.color.colorAccent)
            .show()
}

fun Context.simpleDialog(titleRes: Int, messageRes: String?, okRes: Int) {
    MaterialDialog.Builder(this)
            .titleGravity(GravityEnum.CENTER)
            .contentGravity(GravityEnum.CENTER)
            .buttonsGravity(GravityEnum.CENTER)
            .title(titleRes)
            .content(messageRes ?: getString(R.string.unknown_error))
            .neutralText(okRes)
            .neutralColorRes(R.color.colorAccent)
            .show()
}
