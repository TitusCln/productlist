package com.mobile.tituscln.productlist.product.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobile.tituscln.productlist.R
import com.mobile.tituscln.productlist.product.viewmodel.ProductsViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : Fragment() {

    lateinit var viewModel: ProductsViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(ProductsViewModel::class.java)

        rv_products.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rv_products.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        viewModel.bearer.observe(activity!!, Observer { bearer ->
            viewModel.getProducts(bearer.toString()).observe(activity!!, Observer { prods ->
                prods?.let {
                    rv_products.adapter = ProductsAdapter(it)
                }

            })
        })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe
    fun onDelete(id: Int?) {
        viewModel.deleteProduct(id)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getProducts(viewModel.bearer.value!!)
    }


}
