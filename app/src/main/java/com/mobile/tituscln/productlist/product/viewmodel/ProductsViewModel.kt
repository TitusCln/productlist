package com.mobile.tituscln.productlist.product.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.mobile.tituscln.productlist.AppDatabase
import com.mobile.tituscln.productlist.product.model.Product
import com.mobile.tituscln.productlist.product.model.ProductApi
import com.mobile.tituscln.productlist.utils.SingleLiveEvent
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.AnkoLogger

class ProductsViewModel(application: Application) : AndroidViewModel(application), AnkoLogger {


    private lateinit var token: String

    private val products = MutableLiveData<List<Product>>()
    val error = SingleLiveEvent<String>()
    val bearer = MutableLiveData<String>()
    private var webService: ProductApi? = null


    fun getProducts(token: String): LiveData<List<Product>> {
        webService = ProductApi.create(token)
        loadProducts()
        return products
    }

    private fun loadProducts() {
        webService?.getAllProducts()?.subscribeOn(Schedulers.io())?.subscribe(
                { prods ->
                    products.postValue(prods)
                    AppDatabase.getInstance(getApplication())?.ProductDao()?.insertProducts(prods)
                },
                { errors ->
                    error.postValue(errors.localizedMessage)
                })
    }

    fun deleteProduct(id: Int?) {
        webService?.deleteProduct(id)?.subscribeOn(Schedulers.io())?.subscribe(
                { success ->
                    if (success)
                        loadProducts()
                }
        )
    }

}