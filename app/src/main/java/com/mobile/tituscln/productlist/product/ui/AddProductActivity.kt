package com.mobile.tituscln.productlist.product.ui

import android.Manifest
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mobile.tituscln.productlist.R
import com.mobile.tituscln.productlist.product.viewmodel.AddProductViewModel
import com.mobile.tituscln.productlist.utils.simpleDialog
import kotlinx.android.synthetic.main.activity_add_product.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.File

class AddProductActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {
    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        takePicture()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product)
        val token = intent.extras.getString("token", "")

        val viewModel = ViewModelProviders.of(this).get(AddProductViewModel::class.java)
        viewModel.bearer.value = token
        viewModel.error.observe(this, Observer { error ->
            simpleDialog(R.string.error_message, error, R.string.ok)
        })
        viewModel.success.observe(this, Observer { success ->
            success?.let {
                if (success)
                    finish()
            }
        })
        product_add.setOnClickListener {
            viewModel.onAdd(etName.text.toString(), etDescription.text.toString(), "")
        }

        take_pic.setOnClickListener {
            takePicture()
        }
    }

    @AfterPermissionGranted(RC_CAMERA)
    fun takePicture() {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            EasyImage.openCamera(this, EasyImage.REQ_TAKE_PICTURE)
        } else {
            EasyPermissions.requestPermissions(this, "Necesitas otorgar los siguientes permisos", RC_CAMERA, *perms)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, object : DefaultCallback() {
            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                imageFile?.let {
                    val bitmap = BitmapFactory.decodeFile(it.absolutePath)
                    take_pic.setImageBitmap(bitmap)
                }
            }

        })
    }


    companion object {
        const val RC_CAMERA = 1
    }
}
