package com.mobile.tituscln.productlist.login.model

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface UserDao {

    @Query("select * from User")
    fun getAllUsers(): LiveData<List<User>>

    @Query("select * from User where email=:email")
    fun getUser(email: String): LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    @Delete
    fun deleteUser(user: User)
}