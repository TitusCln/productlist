package com.mobile.tituscln.productlist.product.model

import android.arch.persistence.room.*
import com.mobile.tituscln.productlist.login.model.User
import android.arch.persistence.room.OnConflictStrategy




@Dao
interface ProductDao {

    @Query("select * from Product")
    fun getAllProducts():List<Product>

    @Query("select * from Product where id=:id")
    fun getProduct(id:Long):Product

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(product:Product)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProducts(products: List<Product>)

    @Delete
    fun deleteProduct(product: Product)

}