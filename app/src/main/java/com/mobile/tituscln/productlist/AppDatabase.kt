package com.mobile.tituscln.productlist

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.mobile.tituscln.productlist.login.model.User
import com.mobile.tituscln.productlist.login.model.UserDao
import com.mobile.tituscln.productlist.product.model.Product
import com.mobile.tituscln.productlist.product.model.ProductDao

@Database(version = 1, entities = arrayOf(Product::class, User::class))
abstract class AppDatabase : RoomDatabase() {

    abstract fun ProductDao(): ProductDao
    abstract fun UserDao(): UserDao

    companion object {
        private var mSingleton: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (mSingleton == null) {
                synchronized(AppDatabase::class) {
                    mSingleton = Room.databaseBuilder(context.applicationContext,
                            AppDatabase::class.java,
                            "products.db")
                            .build()
                }
            }

            return mSingleton
        }
    }
}