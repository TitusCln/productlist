package com.mobile.tituscln.productlist.login.model

import android.arch.lifecycle.LiveData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(val api: UserApi, val daoUser: UserDao) {


    fun getUser(user: User): LiveData<User> {
        refreshUser(user)
        return daoUser.getUser(user.email!!)
    }

    fun refreshUser(user: User) {

    }
}